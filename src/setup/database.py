"""
Functions to create a database connection object
"""

import dimension

dim_sdk = dimension.Connect("../src/conf/conf.txt")
db = dim_sdk.db()
